package main

import (
	"fmt"
	"log"
	"os"
	"time"

	vault "github.com/hashicorp/vault/api"
	"github.com/m1/go-generate-password/generator"
	"gopkg.in/yaml.v2"
)

type SecretConfig struct {
	Path string
	Data []struct {
		Key       string
		Length    uint
		Special   bool
		Uppercase bool
	}
}

func config(configFile string) ([]SecretConfig, error) {

	data, err := os.ReadFile(configFile)

	if err != nil {
		log.Printf("Unable to read config file: %v\n", err)
		return nil, err
	}

	secretsConfig := []SecretConfig{}

	err = yaml.Unmarshal(data, &secretsConfig)
	if err != nil {
		log.Printf("Unable to parse YAML: %v\n", err)
		return nil, err
	}

	return secretsConfig, nil
}

func generateSecrets(secretsConfig []SecretConfig, client *vault.Client) error {

	for _, secretConfig := range secretsConfig {
		path := fmt.Sprintf("/secret/data/%s", secretConfig.Path)

		secret, _ := client.Logical().Read(path)

		if secret == nil {
			secretData := map[string]interface{}{
				"data": map[string]interface{}{},
			}

			for _, secretKey := range secretConfig.Data {
				config := generator.Config{
					Length:                     secretKey.Length,
					IncludeSymbols:             secretKey.Special,
					IncludeNumbers:             true,
					IncludeLowercaseLetters:    true,
					IncludeUppercaseLetters:    secretKey.Uppercase,
					ExcludeSimilarCharacters:   true,
					ExcludeAmbiguousCharacters: true,
					CharacterSet:               "abcdef0123456789",
				}
				password, _ := generator.New(&config)

				res, err := password.Generate()
				if err != nil {
					log.Println("Unable to generate password.")
					return err
				}

				secretData["data"].(map[string]interface{})[secretKey.Key] = res
			}

			// The loop allows for Vault to finish startup
			for i := 1 ; i < 10 ; i++ {
				log.Printf("Writing secret '%s': Attempt %d of 10...\n", path, i)
				_, err := client.Logical().Write(path, secretData)

				if err != nil {
					if i == 10 {
						log.Printf("Unable to write secret: %v\n", err)
						return err
					} else {
						delay := time.Duration(i * 30)
						log.Printf("Unable to write secret: %v\n", err)
						log.Printf("Trying again in %d seconds...\n", delay)
						time.Sleep(delay * time.Second)
						continue
					}
				} else {
					log.Println("Secret written successfully.")
					break
				}
			}
		} else {
			log.Println("The specified secret already exists.")
		}
	}

	return nil
}

func main() {

	secretsConfig, err := config("/config/config.yaml")
	if err != nil {
		log.Fatalf("A fatal error has occurred, exiting!\n")
	}

	client, err := vault.NewClient(vault.DefaultConfig())
	if err != nil {
		log.Fatalf("Unable to initialize Vault client: %v\n", err)
	}

	err = generateSecrets(secretsConfig, client)
	if err != nil {
		log.Fatalf("A fatal error has occurred, exiting!\n")
	}
}
