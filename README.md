# Vault - Generate Secrets

[![Latest Tag](https://img.shields.io/gitlab/v/tag/ralgar/vault-generate-secrets?style=for-the-badge&logo=semver&logoColor=white)](https://gitlab.com/ralgar/vault-generate-secrets/tags)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/ralgar/vault-generate-secrets?branch=master&label=Pipeline&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/vault-generate-secrets/-/pipelines)
[![Software License](https://img.shields.io/badge/License-GPL--3.0-orange?style=for-the-badge&logo=gnu&logoColor=white)](https://www.gnu.org/licenses/gpl-3.0.html)
[![GitLab Stars](https://img.shields.io/gitlab/stars/ralgar/vault-generate-secrets?color=gold&label=Stars&logo=gitlab&style=for-the-badge)](https://gitlab.com/ralgar/vault-generate-secrets)

## Overview

This project allows for an easy way to automatically generate secrets using
 Hashicorp Vault and Kubernetes. It is written in Go, statically-linked, and
 released as a [UBI 8 Micro](https://www.redhat.com/en/blog/introduction-ubi-micro)
 container.

## Usage

1. Create a new manifest file, `generate-secrets.yaml`:

   ```yaml
   ---
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: generate-secrets-config
     namespace: vault
   data:
     config.yaml: |
       - path: appgroup/application1
         data:
           - key: apiKey
             length: 32
             special: false
             uppercase: false

       - path: appgroup/application2
         data:
           - key: adminPassword
             length: 16
             special: true
             uppercase: true
           - key: apiKey
             length: 32
             special: false
             uppercase: false
   ---
   apiVersion: batch/v1
   kind: Job
   metadata:
     name: generate-secrets
     namespace: vault
   spec:
     template:
       metadata:
         name: generate-secrets
       spec:
         restartPolicy: Never
         containers:
           - name: apply
             image: registry.gitlab.com/ralgar/vault-generate-secrets:v1.0.0
             env:
               - name: VAULT_ADDR
                 value: http://vault:8200
               - name: VAULT_TOKEN
                 valueFrom:
                   secretKeyRef:
                     name: vault-unseal-keys
                     key: vault-root
             volumeMounts:
               - name: generate-secrets-config
                 mountPath: /config
         volumes:
           - name: generate-secrets-config
             configMap:
               name: generate-secrets-config
   ```

1. Edit the values as needed.
1. Apply it to your cluster:

   ```
   kubectl apply -f generate-secrets.yaml
   ```

## Acknowledgments

- [Khue Doan](https://github.com/khuedoan/homelab) - For the original code,
   which I modified and refactored to better suit my purposes.

## License

Copyright: (c) 2022, Ryan Algar ([ralgar](https://gitlab.com/ralgar/homelab))

GNU General Public License v3.0 (see LICENSE or [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt))
